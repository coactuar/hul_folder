-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 10:37 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coacteh9_hul`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`) VALUES
(8, 'Amit', 'amit@hexagonevents.com', 'Hello Demo Question', '2020-01-08 15:50:17', 'iitm-test'),
(9, 'Ezhumalai', 'Ezhumalai.perumal@unilever.com', 'hi', '2020-01-08 15:58:55', 'iitm-test'),
(10, 'pooja', 'pooja@hexagonevents.com', 'Demo Question', '2020-01-08 16:04:20', 'iitm-test'),
(11, 'yogesh', 'yogesh.menon@unilever.com', 'hi', '2020-01-08 16:49:29', 'iitm-test'),
(12, 'yogesh', 'yogesh.menon@unilever.com', 'no video', '2020-01-08 16:49:38', 'iitm-test');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(4, 'pawan shilwant', 'pawan@coact.co.in', '2020-01-08 15:36:28', '2020-06-23 16:27:40', '2020-06-23 16:28:10', 1, 'iitm-test'),
(5, 'Akshat Jharia', 'akshat@coact.co.in', '2020-01-08 15:41:17', '2020-01-09 15:43:25', '2020-01-09 15:43:55', 1, 'iitm-test'),
(6, 'Amit', 'amit@hexagonevents.com', '2020-01-08 15:45:25', '2020-01-09 14:13:22', '2020-01-09 14:13:52', 1, 'iitm-test'),
(7, 'Ezhumalai', 'Ezhumalai.perumal@unilever.com', '2020-01-08 15:48:05', '2020-01-09 15:43:21', '2020-01-09 15:43:51', 1, 'iitm-test'),
(8, 'Sujatha', 'sujatha@coact.co.in', '2020-01-08 15:51:24', '2020-01-08 16:12:39', '2020-01-08 16:50:34', 0, 'iitm-test'),
(9, 'pooja', 'pooja@hexagonevents.com', '2020-01-08 16:03:18', '2020-01-09 13:13:40', '2020-01-09 13:14:10', 1, 'iitm-test'),
(10, 'Pooja', 'pooja@coact.co.in', '2020-01-08 16:11:26', '2020-01-08 16:11:26', '2020-01-08 16:11:56', 1, 'iitm-test'),
(11, 'yogesh', 'yogesh.menon@unilever.com', '2020-01-08 16:49:04', '2020-01-08 16:50:31', '2020-01-08 16:51:01', 1, 'iitm-test'),
(12, 'Pooja', 'pooja.jaiswal@gmail.com', '2020-01-09 08:33:46', '2020-01-09 17:34:20', '2020-01-09 17:34:50', 1, 'iitm-test'),
(13, 'test', 'test@test.com', '2020-01-09 09:31:13', '2020-01-09 14:22:17', '2020-01-09 14:22:47', 1, 'iitm-test'),
(14, 'Tavan', 'Tavan.m@unilever.com', '2020-01-09 09:56:09', '2020-01-09 15:00:21', '2020-01-09 15:00:51', 1, 'iitm-test'),
(15, 'Akshat Jharia', 'akshat.jharia@itorizon.com', '2020-01-09 10:01:48', '2020-01-09 10:01:48', '2020-01-09 10:02:18', 1, 'iitm-test'),
(16, 'yahya.jagrala', 'yahya.jagrala@unilever.com', '2020-01-09 10:32:31', '2020-01-09 14:12:50', '2020-01-09 14:13:20', 1, 'iitm-test'),
(17, 'Akshat Jharia', 'akshatjharia@gmail.com', '2020-01-09 11:55:05', '2021-06-30 13:08:04', '2021-06-30 13:08:34', 1, 'iitm-test'),
(18, 'Pooja Jaiswal', 'pooja93jaiswal@gmail.com', '2020-01-09 12:28:20', '2020-01-09 16:01:02', '2020-01-09 16:01:32', 1, 'iitm-test'),
(19, 'Sujatha ', 'aujatha@coact.co.in', '2020-01-09 13:19:04', '2020-01-09 13:21:56', '2020-01-09 13:22:26', 1, 'iitm-test'),
(20, 'Ravi Khot', 'viraj@coact.co.in', '2020-01-09 13:21:21', '2020-01-09 13:21:21', '2020-01-09 13:21:51', 1, 'iitm-test'),
(21, 'Resha.kothari', 'Resha.kothari@unilever.com', '2020-01-09 13:27:22', '2020-01-09 14:03:28', '2020-01-09 14:03:58', 1, 'iitm-test'),
(22, 'Mahesh', 'mahesh@coact.co.in', '2020-01-09 17:34:51', '2020-01-09 17:34:51', '2020-01-09 17:35:16', 0, 'iitm-test'),
(23, 'shyam', 'shyam0987@gmail.com', '2020-04-21 11:59:23', '2020-04-21 11:59:23', '2020-04-21 11:59:53', 1, 'iitm-test');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
