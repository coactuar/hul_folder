<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>HUL :: Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid main">
    <div class="row header">
        <div class="col-12 col-md-2">
            <img src="img/logo.png" class="img-fluid logo" alt=""/> 
        </div>
        
        
    </div>
    <div class="row mt-5">
        <div class="col-10 col-md-6 col-lg-4 offset-1 offset-md-3 offset-lg-4">
            <div class="login">
                <h3>Enter your details</h3>
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Your Name" aria-label="Your Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Your Email" aria-label="Your Email" aria-describedby="basic-addon1" name="email" id="email" required>
                  </div>
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                
            </form>
            </div>
        
        </div>
    </div>
    <div class="coact-logo d-none d-sm-block"><img src="img/coact-logo.jpg" alt=""/></div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert-danger').fadeIn().delay(3000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>